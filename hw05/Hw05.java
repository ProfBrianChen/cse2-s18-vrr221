// Victoria Raso, CSE2, 3/6/18
// ask for the course number, department name, the number of times it meets in a week,
// the time the class starts, the instructor name, and the number of students.

import java.util.Scanner;

public class Hw05 {
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in);
    
    // get the correct course number
    System.out.println("Enter the course number ");  
    while (!scan.hasNextInt()){
      scan.next();
      System.out.println("Please enter the course number ");
    }
    int courseNum = scan.nextInt();
    
    // get the correct department name
    System.out.println("Enter the department name ");
    while (scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter the department name ");
      if (true){
        scan.next();
        break;
      }
    }
    String depName = scan.nextLine();
    
    // get the correct number of times it meets in a week
    System.out.println("Enter how many times you meet a week ");
    while (!scan.hasNextInt()){
      scan.next();
      System.out.println("Please enter the amount of times you meet a week ");
    }
    int timesPerWeek = scan.nextInt();
    
    // get the correct time the class starts 
    System.out.println("Enter the time the class starts including a colon and specify am or pm ");
    while (scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter the time class starts ");
      if (true){
        scan.next();
        break;
      }
    }
    String classStart = scan.nextLine();
    
    // get the correct instructor's name
    System.out.println("Enter the instructor's name ");
    while (scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter the instructor's name ");
      if (true){
        scan.next();
        break;
      }
    }
    String instructor = scan.nextLine();
    
    // get the correct number of students in the class
    System.out.println("Enter the amount of students in the class ");
    while (!scan.hasNextInt()){
      scan.next();
      System.out.println("Please enter the amount of students in the class ");
    }
    int numOfStudents = scan.nextInt();
  
  }
}
