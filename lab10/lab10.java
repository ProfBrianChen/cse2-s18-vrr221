// Victoria Raso, CSE2

public class lab10 {

	public static void main (String[] args)
	{
		int r1 = (int)(Math.random()*11);
		int r2 = (int)(Math.random()*11);
		int r3 = (int)(Math.random()*11);
		int r4 = (int)(Math.random()*11);
		int[][] a = increasingMatrix(r1,r2,true);
		printMatrix(a, false);
		System.out.println();
		int[][] b = increasingMatrix(r1,r2,false);
		printMatrix(b, true);
		System.out.println();
		int[][] c = increasingMatrix(r3,r4,true);
		printMatrix(c, true);
		System.out.println();
		printMatrix(addMatrix(a, true, b, false),true);
		printMatrix(addMatrix(a, true, c, true),true);
			
	}
	
	
	public static int[][] increasingMatrix(int width, int height, boolean format)
	{
		int[][] matrix = new int[height][width];
		int counter = 0;
		if (format)
		{
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					matrix[i][j] = ++counter;
				}
			}
		}
		else
		{
			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					matrix[j][i] = ++counter;
				}
			}
		}
		return matrix;
	}
	
	public static void printMatrix (int[][] array, boolean format)
	{
		if (array == null) {
			System.out.println("The array was empty!");
			return;
		}
		for (int i = 0; i < array.length; i++)
		{
			for (int j = 0; j < array[i].length; j++)
			{
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}
	public static int[][] translate(int[][] array) //coumn major -> row major
	{
		int[][] row_major = new int[array[0].length][array.length];
		for (int i = 0; i < array[0].length; i++)
		{
			for (int j = 0; j < array.length; j++)
			{
				row_major[i][j] = array[j][i];
			}
		}
		return row_major;
	}
	public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb)
	{
		int[][] sum_array = new int[a.length][a[0].length];
		if(a.length != b.length && a[0].length != b[0].length)
		{
			System.out.println("The arrays cannot be added");
			return null;
		}
		for (int i = 0; i < a.length; i++)
		{
			for (int j = 0; j < a[i].length; j++)
			{
				sum_array[i][j] = a[i][j] + b[i][j];
			}
		}
		return sum_array;
	}
	
	
}
