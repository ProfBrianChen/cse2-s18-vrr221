// Victoria Raso, 2/9/18, CSE2
// This program will allow the user to enter the check total, tip they wish to pay, and amount of people paying
// and calculate how much each person should pay
import java.util.Scanner;

public class Check {
  // main method
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in ); // declare an instance of the Scanner object and call the Scanner constructor
    
    // prompt the user for the original cost of the check 
    System.out.print("Enter the original cost of the check in the form xx.xx: "); 
    // accept user input for cost of check
    double checkCost = myScanner.nextDouble();
    
    // prompt the user for the tip percentage that they wish to pay
    System.out.print("Enter the percentage tip that you wish to py as a whole number in the form xx: ");
    // accept user input for tip percentage
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; // converts the percentage into a decimal value
      
    // prompt the user for the number of people that went to dinner 
    System.out.print("Enter the number of people who went out to dinner: ");
    // accept user input for number of people at dinner
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; // whole dollar amount of cost; stores digits to the right of the decimal point for the cost$ 
    
    totalCost = checkCost * (1 + tipPercent); // total cost including tip
    costPerPerson = totalCost / numPeople; // cost each person pays
    dollars = (int)costPerPerson; // gets the whole amount, dropping decimal fraction
    dimes=(int)(costPerPerson * 10) % 10; // gets the 10's place for the cents each person owes
    pennies=(int)(costPerPerson * 100) % 10; // gets the 1's place for the cents each person owes
    // print out what each person owes 
    System.out.print("Each person in the group owes $" + dollars + "." + dimes + pennies);

      
  } // end of main method
} // end of class