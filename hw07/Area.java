// Victoria Raso, CSE2, 3/27/18
// calculate the area of a rectangle, triangle, or circle

import java.util.Scanner;

public class Area {
  public static void rectArea(){ // calculates the area of a rectangle
    double area = 0;
    System.out.println("Enter a length ");
    double length = input();
    System.out.println("Enter a width ");
    double width = input();
    area = length*width;
    System.out.println("area: " + area);
  }
  public static void triArea(){ // calculates the area of a triangle
    double area = 0;
    System.out.println("Enter a base ");
    double base = input();
    System.out.println("Enter a height ");
    double height = input();
    area = base*height/2;
    System.out.println("area: " + area);
  }
  public static void circArea(){ // calculates the area of a circle
    double area = 0;
    System.out.println("Enter a radius ");
    double radius = input();
    area = Math.PI*Math.pow(radius, 2);
    System.out.println("area: " + area);
  }
  public static double input(){
    double x = 0;
    Scanner scan = new Scanner(System.in);
     while (!scan.hasNextDouble()){
      scan.next();
      System.out.println("Please make sure it's a valid number. ");
    }
    x = scan.nextDouble();
    return x;
  }
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    String shape = "";
    boolean flag = true;
    while (flag){
      System.out.println("Enter the shape you'd like calculate the area of: rectangle, triangle, or circle");
      shape = scan.nextLine();
      if (shape.equals("rectangle") || shape.equals("triangle") || shape.equals("circle"))
        flag = false;
      else
        System.out.println("Please enter a valid shape.");
    }
    
    if (shape.toLowerCase().equals("circle"))
      circArea();
    if (shape.toLowerCase().equals("triangle"))
      triArea();
    if (shape.toLowerCase().equals("rectangle"))
      rectArea();
    
    
  }  
}