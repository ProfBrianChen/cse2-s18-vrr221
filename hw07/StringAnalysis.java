// Victoria Raso, CSE2, 3/27/18
// examine all or a specified number of characters in a string and determine if they are letters

import java.util.Scanner;

public class StringAnalysis{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter a string");
    String stringInput = scan.next();
    System.out.println("Do you want to check the whole string: y/n?");
    String whole = scan.next();
    
    boolean isLetter = false; // declare boolean value
    
    while (!whole.toLowerCase().equals("y") && !whole.toLowerCase().equals("n")){
      System.out.println("Please respond with y or n");
      whole = scan.next();
    }
    int userLength = 0;

    if (whole.toLowerCase().equals("n"))
    {
      System.out.println("Enter how much of the string you want to evaluate");
      userLength = scan.nextInt();
      isLetter = Examine(stringInput, userLength);
    }
    else if (whole.toLowerCase().equals("y")) 
    {
     isLetter = Examine(stringInput);
    }  
   
  }
  
  public static boolean Examine(String s, int l){ // method that a string and an int
    boolean isLetter; // declare boolean
    if (l < s.length()){
      l = l;
    }
    else{
      l = s.length();
    }
    
    for (int i = 0; i < l; i++){
      if (s.charAt(i).isLetter()){
        isLetter = true; // sets boolean variable to be true
      }
      else {
        isLetter = false; // sets boolean variable to be false
      }
      return isLetter; // output
    }
    
    
  }
 
  public static boolean Examine(String s){ // method that just accepts a string
    boolean isLetter; 
    for (int k = 0; k < s.length(); k++){
      if (s.charAt(k).isLetter()){
        isLetter = true; // sets boolean variable to true
      }
      else{
        isLetter = false; // sets boolean variable to false
      }
    }
    return isLetter; // output
  }
    
}