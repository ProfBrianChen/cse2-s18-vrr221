// Victoria Raso, CSE2, 4/24/18
// represent a city array w/ 2D array of ints and have robots invade cities w/in the array of random populations

public class RobotCity {

	public static void main (String[] args)
	{
		System.out.println("City:");
		int[][] city = buildCity(); // calls the build city method to create the city array
		display(city); // prints the random populations of the random number of cities
		int k = (int)(Math.random()*50)+1; // number of robots invading
		city = invade(city, k); // calls the invade method based on the city array and the number of robots
		System.out.println("\nInvade:");
		display(city); // calls the display method to print the invaded city
		for (int i = 0; i < 5; i++) // updates and prints the robots moving east 5 times
		{
			city = update(city);
			System.out.println("\nUpdate:");
			display(city);
		}
		
	}
	
	public static int[][] buildCity()
	{
		int east_west = (int)(Math.random() * 6)+10; // creates a random number of columns between 10 and 15
		int north_south = (int)(Math.random() * 6)+10; // creates a random number of rows between 10 and 15
		int[][] city = new int[north_south][east_west]; // creates array with these random boundaries
		for(int i = 0; i < city.length; i++)
		{
			for (int j = 0; j < city[i].length; j++)
			{
				city[i][j] = (int)(Math.random() * 900)+100; // assigns each member in the 2D array a value between 100 and 999
			}
		}
		return city;
	}
	
	public static void display(int[][] matrix)
	{
		for(int i = 0; i < matrix.length; i++)
		{
			for (int j = 0; j < matrix[i].length; j++)
			{
				System.out.print(matrix[i][j] + " "); // prints each value of the 2D array in the proper row and column
			}
			System.out.println();
		}
	}
	
	public static int[][] invade(int[][] city, int k)
	{
		int row, col;
		
    boolean flag = true;
		for (int i = 0; i < k; i++) // each robot invades one city in the array 
		{
			do {
				flag = true;
				row = (int)(Math.random()* city.length); // generates a random row in the array to invade
				col = (int)(Math.random()* city[row].length); // generates a random column in the array to invde
				if (city[row][col] > 0)
				{
					city[row][col] *= -1;
					flag = false; // if the city was already invaded/is already negative, loop will not run and the robot will choose another city
				}
			}
			while(flag);

		}
		return city;
	}
	
	public static int[][] update(int[][] city)
	{
		int[][] updated_city = new int[city.length][city[0].length];
		for(int i = 0; i < city.length; i++)
		{
			for (int j = 0; j < city[i].length; j++)
			{
				updated_city[i][j] = city[i][j]; // creates a copy of the city array in the array updated_city
			}
		}
		
		for(int i = 0; i < city.length; i++)
		{
			for (int j = 0; j < city[i].length; j++)
			{
				if (city[i][j] < 0) // if the city has already been invaded and is therefore negative
				{
					updated_city[i][j] *= -1; // this makes the member in the array positive again
					if (j+1 < city[i].length && city[i][j+1] > 0) // this checks to make sure the point in the array is not too far east
						updated_city[i][j+1] *= -1; // invades the next city
          else if (j+1 < city[i].length && (city[i][j] < 0 && city[i][j] < 0))
            updated_city[i][j+1] *= -1; // if there are mulitiple invasions right next to each other, this makes sure that the negative numbers stay in the right places as the invasions move to the east
				}
			}
		}
		return updated_city; // returns the next invasion
	}
		
}