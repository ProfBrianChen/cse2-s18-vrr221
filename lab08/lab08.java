// Victoria Raso, CSE2, 4/6/18
// create an array of a randomized size from 5 to 10, ask the user to fill the array with different strings using a scanner
// create a second array midterm, fill with random integers 0 to 100, print out the members of the two arrays 

import java.util.Scanner;

public class lab08{
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in); // since the user is entering names, we need a scanner 
    
    // randomized number from 5 to 10
    int randomizedNum = (int)(Math.random() * 5) + 6;
    System.out.println(randomizedNum);
    
    // declare an array for student names 
    String[] students;
    // allocate the array based on the random number
    students = new String[randomizedNum]; // randomizedNum is the array length
    // initialize variable; assign values to array indexes
    System.out.println("Enter " + randomizedNum + " student names:");
    for (int i = 0; i < randomizedNum; i++){
      students[i] = scan.nextLine();
    }
    
    // declare an array for midterm grades
    int[] midterm;
    // allocate the array based on the random number that was used for students as well
    midterm = new int[randomizedNum]; // randomizedNum is the array length
    // initialize variable; assign values to array indexes
    System.out.println("Here are the midterm grades of the " + randomizedNum + " students above:");
    for (int i = 0; i < randomizedNum; i++){
      midterm[i] = (int)(Math.random() * 101);
      System.out.println(students[i] + " : " + midterm[i]);
    }
    
   
  }
}