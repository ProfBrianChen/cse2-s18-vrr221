// Victoria Raso, CSE2, 3/2/18
// create a pattern by entering a length

import java.util.Scanner;

public class TwistGenerator {
  // main method 
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in); // declare an instance of the Scanner object 
    
    // ask the user for a positive integer called "length"   
    System.out.println("Enter a positive integer for length");
    int length = 0;
    // if the user does not provide an integer, ask again in a while loop
    while (true){
      if (!scan.hasNextInt()){
        scan.next();
        System.out.println("Enter an integer");
        continue;
      }
      else {
        length = scan.nextInt();
        if (length > 0){
          break;
        }
        else{
          System.out.println("Enter a postive integer");
        }
      }
    }
    System.out.println("out of while loop");
           
        
    int numberA = length/3; // says how many groupings there will be of the pattern
    int numberB = length-numberA;  
    int numberC = numberB%2; // checks if numbmerC is even 
    
    int counter = 0;
    while (counter < numberA){
      System.out.print("\\ /"); 
      counter++;
    }
    if (numberC != 0){
      System.out.print("\\"); // prints last slash if length is not even
    }
    System.out.println(); // creates new line
    
    int counter2 = 0;
    while (counter2 < numberA){
      System.out.print(" X ");
      counter2++;
    }
    System.out.println(); // creates new line
    
    int counter3 = 0;
    while (counter3 < numberA){
      System.out.print("/ \\");
      counter3++;
    }
    if (numberC != 0){
      System.out.print("/"); // prints last slash if length is not even
    }      
                                                          
  }
}