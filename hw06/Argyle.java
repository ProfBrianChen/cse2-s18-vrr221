// Victoria Raso, CSE2, 3/20/18
// print out argyle design

import java.util.Scanner;

public class Argyle {
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);
    
    // make sure the user enters a width of viewing window in characters 
    System.out.println("Enter a positive integer for the width of Viewing window in characters ");
    while (!scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter a positive integer for the width of Viewing window in characters ");
      if (true){
        scan.next();
        break;
      }
    }
    String width = scan.nextLine();
    
    // make sure the user enters a height of viewing window in characters
    System.out.println("Enter a positive integer for the height of the Viewing window in characters ");
    while (!scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter a positive integer for the height of the Viewing window in characters ");
      if (true){
        scan.next();
        break;
      }
    }
    String height = scan.nextLine();
    
    // make sure the user enters a width of the argyle diamonds 
    System.out.println("Enter a positive integer for the width of the argyle diamonds ");
    while (!scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter a positive integer for the width of the argyle diamonds ");
      if (true){
        scan.next();
        break;
      }
    }
    String argyleWidth = scan.nextLine();
    
    // make sure the user enters an odd integer for the width of the argyle center stripes
    System.out.println("Enter a positive odd integer for the width of the argyle center stripes ");
    while (!scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter a positive odd integer for the width of the argyle center stripes ");
      if (true){
        scan.next();
        break;
      }
    }
    String stripeWidth = scan.nextLine();
    
    // make sure the user enters a character for the pattern
    System.out.println("Enter a first character for the pattern fill ");
    while (!scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter a first character for the pattern fill ");
      if (true){
        scan.next();
        break;
      }
    }
    String firstChar = scan.nextLine();
    
    // make sure the user enters a second character for the pattern
    System.out.println("Enter a second character for the pattern fill ");
    while (!scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter a second character for the pattern fill ");
      if (true){
        scan.next();
        break;
      }
    }
    String secondChar = scan.nextLine();
    
    // make sure the user enters a third character for the pattern
    System.out.println("Enter a third character for the stripe fill ");
    while (!scan.hasNextLine()){
      scan.next();
      System.out.println("Please enter a third character for the stripe fill ");
      if (true){
        scan.next();
        break;
      }
    }
    String thirdChar = scan.nextLine();
    
    int widthInInt = Integer.valueOf(width);
    System.out.println(widthInInt);
    int heightInInt = Integer.valueOf(height);
    System.out.println(heightInInt);
    int argyleWidthInInt = Integer.valueOf(argyleWidth);
    //System.out.println(argyleWidthInInt);
    int stripeWidthInInt = Integer.valueOf(stripeWidth);
    
    //System.out.println("Start the pattern");
    
    
    int x = 0;
    while (x < heightInInt)
		{
			
				for (int i = 0; i < argyleWidthInInt; i++)
				{
					for (int j = 0; j <= argyleWidthInInt; j++) //top left block
					{
						if (i <= argyleWidthInInt- j)
						{
							System.out.print(firstChar);
						}
						if (i > argyleWidthInInt - j)
						{
							System.out.print(secondChar);
						}
						if (i == j)
						{
							System.out.print(thirdChar);
						}
					}

					for (int j = 0; j <= argyleWidthInInt; j++) //top right block
					{
						if (i <= j)
						{
							System.out.print(firstChar);
						}
						if (i > j)
						{
							System.out.print(secondChar);
						}
						if (i + j == argyleWidthInInt)
						{
							System.out.print(thirdChar);
						}
					}
				
					System.out.println();
					x++;
				
				}
				for (int i = 0; i < argyleWidthInInt; i++) //bottom left block
				{
					for (int j = 0; j <= argyleWidthInInt; j++)
					{
						if (i >= j)
						{
							System.out.print(firstChar);
						}
						if (i < j)
						{
							System.out.print(secondChar);
						}
						if (i + j == argyleWidthInInt)
						{
							System.out.print(thirdChar);
						}
					}
					for (int j = 0; j <= argyleWidthInInt; j++) //bottom right block
					{
						if (i >= argyleWidthInInt - j)
						{
							System.out.print(firstChar);
						}
						if (i < argyleWidthInInt - j)
						{
							System.out.print(secondChar);
						}
						if (i == j)
						{
							System.out.print(thirdChar);
						}
					}
					System.out.println();
					x++;
				}
				x++;
			
		}	
    
    
  

    // QUESTION: WHY IS IT NOT STOPPING AT THE HEIGHT THAT I TYPE IN???
    
  }
}