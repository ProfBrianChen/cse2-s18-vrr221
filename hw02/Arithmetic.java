// Victoria Raso, 2/4/18, CSE2

public class Arithmetic {
  public static void main(String[] args) {
   
    // input variables
    // number of pairs of pants
    int numPants = 3;
    // cost per pair of pants
    double pantsPrice = 34.98;
    
    // number of sweatshirts
    int numShirts = 2;
    // cost per shirt
    double shirtPrice = 24.99;
    
    // number of belts 
    int numBelts = 1;
    // cost per belt
    double beltCost = 33.99;
    
    // tax rate
    double paSalesTax = 0.06;

   
     // costs to be calculated:
    // 1. total cost of each item 
    // 2. sales tax charged buying all of each kind of item
    // 3. total cost of purchases before tax
    // 4. total sales tax
    // 5. total paid for transaction, including sales tax
    
    // variables for each of these values
    double totalCostOfPants; // cost of pants
    double totalCostOfShirts; // cost of shirts
    double totalCostOfBelts; // cost of belts
    
    double salesTaxOfPants; // sales tax charged on pants
    double salesTaxOfShirts; // sales tax charged on shirts
    double salesTaxOfBelts; // sales tax charged on belts
    
    double costOfPurchase; // total cost of purchase before sales tax
   
    double totalSalesTax; // total sales tax of every item
    
    double totalPaid; // total paid for transaction, including sales tax 
    
    // assign values to these variables 
    totalCostOfPants = pantsPrice*(double)numPants; // value for cost of pants
    totalCostOfShirts = shirtPrice*(double)numShirts; // value for cost of shirts
    totalCostOfBelts = beltCost*(double)numBelts; // value for cost of belts
    
    salesTaxOfPants = ((int)(paSalesTax*totalCostOfPants*100))/100.0; // value for sales tax on pants with 2 decimal spaces
    salesTaxOfShirts = ((int)(paSalesTax*totalCostOfShirts*100))/100.0; // value for sales tax on shirts with 2 decimal spaces
    salesTaxOfBelts = ((int)(paSalesTax*totalCostOfBelts*100))/100.0; // value for sales tax on belts with 2 decimal spaces
    
    costOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // value for cost of purchase before sales tax
    
    totalSalesTax = ((int)((salesTaxOfPants + salesTaxOfShirts + salesTaxOfBelts)*100))/100.0; // value for total sales tax of every item with 2 decimal spaces
    
    totalPaid = ((int)((costOfPurchase + totalSalesTax)*100))/100.0; // value for total paid for transaction, including sales tax
    
    // display all the values calulated
    System.out.println("Total cost of pants: $" + totalCostOfPants);
    System.out.println("Total sales tax on pants: $" + salesTaxOfPants);
    
    System.out.println("Total cost of shirts: $" + totalCostOfShirts);
    System.out.println("Total sales tax on shirts: $" + salesTaxOfShirts);
    
    System.out.println("Total cost of belts: $" + totalCostOfBelts);
    System.out.println("Total sales tax on belts: $" + salesTaxOfBelts);
    
    System.out.println("Total cost of purchase, before sales tax: $" + costOfPurchase);
    System.out.println("Total sales tax on purchase: $" + totalSalesTax);
    System.out.println("Total cost of purchase, including sales tax: $" + totalPaid);

  }
}
    