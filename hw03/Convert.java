// Victoria Raso, 2/13/18, CSE2
// Write a program that asks the user for doubles that represent the number of acres of land affected by hurricane precipitation
// and how many inches of rain were dropped on average
// Convert the quantity of rain into cubic miles

import java.util.Scanner; 

public class Convert {
  
   public static void main(String[] args) {
  
     Scanner myScanner = new Scanner( System.in ); // declare an instance of the Scanner object and call the Scanner constructor
  
     // prompt the user to enter the amount of acres affected 
     System.out.print("Enter the affected area in acres: ");
     // accept user input for acres
     double acres = myScanner.nextDouble();
  
     // prompt the user to enter the amout of inches of rainfall
     System.out.print("Enter the rainfall in the affected area: ");
     // accept user input for inches 
     double inches = myScanner.nextDouble();
     
     // according to online conversions, 1 acre x inch units = 102 790.153 liters
     double litersConversion = 102790.153;
       
     // according to online conversions, 1 liter = 0.264172 gallons
     double gallonsConversion = 0.264172;
     
     // according to online conversions, 1 gallon = 9.08169e-13 cubic miles 
     double cubicMilesConversion = 9.08169e-13;
     
     // calculate the total amount of cubic miles affected based on the users inputs and set conversion amounts
     double totalCubicMiles = acres * inches * litersConversion * gallonsConversion * cubicMilesConversion;
     
     // print the cubic miles affected
     System.out.print(totalCubicMiles + " cubic miles");
     
     // I realized that my cubic miles calculated does not match the example shown using the same values for acres and inches
     // however, based on online calculators and set units and conversions, this is the only value that makes sense to me
  
  }
}