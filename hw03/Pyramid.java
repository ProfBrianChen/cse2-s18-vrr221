// Victoria Raso, 2/13/18, CSE2
// Write a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid

import java.util.Scanner; 

public class Pyramid { 

   public static void main(String[] args) {
     
     Scanner myScanner = new Scanner( System.in ); // declare an instance of the Scanner object and call the Scanner constructor
     
     // prompt the user to enter the square side of the pyramid
     System.out.print("The square side of the pyramid is (input length): ");
     // accept input from user for length of square side
     double squareSide = myScanner.nextDouble();
     
     // prompt the user to enter the height of the pyramid
     System.out.print("The height of the pyramid is (input height): ");
     // accept input from user for height of pyramid
     double height = myScanner.nextDouble();
     
     // the equation for volume of a pyramid is lwh/3
     // length and width are equal since the base is a square
     
     // calculate the volume of the pyramid
     double volume = (squareSide*squareSide*height/3);
     // since the homework doesn't specify how the volume should be displayed, all the decimal points will be shown 
     
     // print the volume of the pyramid
     System.out.print("The volume inside the pyramid is: " + volume);
     
   }
}