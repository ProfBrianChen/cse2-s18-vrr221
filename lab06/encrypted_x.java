// Victoria Raso, CSE2, 3/9/18
// hide secret message x

import java.util.Scanner;

public class encrypted_x{
  public static void main(String[] args){
    
    Scanner scan = new Scanner(System.in);
    
    // ask for an input between 0 and 100
    System.out.println("Enter an integer between 0 and 100");
    int input = 0;
    // validate input
    while (true)
    { // checks if input is an integer
      if (!scan.hasNextInt())
      { // if not an integer, it asks again
        scan.next();
        System.out.println("Enter an integer");
        continue;
      }
      else
      { // if it is an integer, user enters an input
        input = scan.nextInt();
        if (input > 0 & input < 100)
        { // if the integer is between 0 and 100, we can stop the loop
          break;
        }
        else
        { // if input is not between 0 and 100, it asks again
          System.out.println("Enter an input between 0 and 100");
        }
      }
    }
    System.out.println("out of while loop");
    
    for(int i = 0; i < input; i++)
    {

      for(int j = 0; j < input; j++)
      {
        if (i == j) // prints upward diagonal
        {
          System.out.print(" ");
        }
        else if (i == input-j-1) // prints downward diagonal
        {
          System.out.print(" ");
        }
        else 
        {
          System.out.print("*");
        }
      }
      System.out.println();
    }
    
  }
}