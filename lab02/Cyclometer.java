// Victoria Raso, February 2nd, CSE2
// print the number of minutes for each trip
// print the number of counts for each trip
// print the distance of each trip in miles
// print the distance for the two trips combined
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      int secsTrip1 = 480;  // number of seconds for trip 1
      int secsTrip2 = 3220;  // number of seconds for trip 2
	    int countsTrip1 = 1561;  // number of counts for trip 1
 	    int countsTrip2 = 9037; // number of counts for trip 2
      
      double wheelDiameter = 27.0;  // constant diameter of the wheel
  	  double pi = 3.14159; // constant value of pi
  	  int feetPerMile = 5280;  // constant number of feet in a mile 
  	  int inchesPerFoot = 12;   // constant number of inches in a foot
  	  int secondsPerMinute = 60;  // constant number of seconds in a minute
	    double distanceTrip1, distanceTrip2, totalDistance;  // output data for distance of the trips
      
      // print out the numbers stored in the variables that store number of seconds and the counts
    
      System.out.println("Trip 1 took " + (double)(secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
      System.out.println("Trip 2 took " + ((double)secsTrip2/(double)secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
      
      //run the calculations; store the values
      
      distanceTrip1=countsTrip1*wheelDiameter*pi;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
      
      distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
      distanceTrip2=countsTrip2*wheelDiameter*pi/inchesPerFoot/feetPerMile;
      totalDistance=distanceTrip1+distanceTrip2;
      
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");

    } // end of main method
} // end of class