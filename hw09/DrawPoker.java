import java.util.Random;
public class DrawPoker{
  
  public static void shuffle(int[] arr){ // method to shuffle the deck of cards
    int rand_index;
    int rand_element;
    
    Random random = new Random();
    
    for (int i = 0; i < arr.length; i++){ // switches the index in the array with a random member in the array
      rand_index = random.nextInt(i+1);
      rand_element = arr[rand_index];
      arr[rand_index] = arr[i];
      arr[i] = rand_element;
    }
  }
  
  public static void printCards(int[] array) { // method to print the number and suit to whatever card number is chosen from the deck  
		for (int i = 0; i < array.length; i++)
		{
			if (array[i]%13 == 0)
				System.out.print("Ace"); // if the remainder is 0, the card value is an Ace
			else if (array[i]%13 == 10)
				System.out.print("Jack"); // if the remainder is 10, the card value is a Jack
			else if (array[i]%13 == 11)
				System.out.print("Queen"); // if the remainder is 11, the card value is a Queen
			else if (array[i]%13 == 12)
				System.out.print("King"); // if the remainder is 12, the card value is a Kind
			else
				System.out.print((array[i]%13)+1); // otherwise, the value of the card is one more than the remainder 
			if (array[i] / 13 == 0)
				System.out.print(" of Diamonds   "); // the first set of 13 cards are diamonds
			else if (array[i] / 13 == 1)
				System.out.print(" of Clubs   "); // the next set of 13 cards are clubs
			else if (array[i] / 13 == 2)
				System.out.print(" of Hearts   "); // the next set of 13 cards are hearts
			else
				System.out.print(" of Spades   "); // the last set of 13 cards are spades
		}
		System.out.println();
	}
  public static void main(String[] args){
    // these are all the options for cards in a deck 
		int[] allCards = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51};
    // call the shuffle function to shuffle the deck of cards
    shuffle(allCards);
    //printCards(allCards);
    
    int[] player1 = new int[5]; // one hand is 5 cards
    int[] player2 = new int[5]; // one hand is 5 cards
    
    for (int i = 0; i < 10; i++){
      if (i%2 == 0){
        player1[i/2] = allCards[i]; // player1 gets every other card starting with the first one in the shuffled deck until they have 5
      }
      else{
        player2[i/2] = allCards[i]; // player2 gets every other card starting with the second one in the shuffled deck until they have 5
      }
    } // deals each player every other cards until they have a full hand
    System.out.print("Player 1's hand: ");
		printCards(player1); // prints player1's hand
    System.out.print("Player 2's hand: ");
		printCards(player2); // prints player2's hand 
    
    Win(player1, player2); // calls the method to see who won
  }
  
  
  public static boolean Pair(int[] arr){ // checks if there are 2 of the same value cards in a hand
    for (int i = 0; i < arr.length; i++){
      for (int j = 0; j < arr.length; j++){ // compares the first card to every other card, then the second card to every other one, so on
        if ((arr[i]%13 == arr[j]%13) && (i != j)) // %13 so it can see if there is a pair no matter what suit
          return true; // returns true if there is a pair
      }
    }
    return false; // returns false if there is no pair
  }
  
  public static boolean ThreeOfAKind(int[] arr){
    for (int i = 0; i < arr.length; i++){
      for (int j = 0; j < arr.length; j++){
        for (int k = 0; k < arr.length; k++){ // compares the first card to every other card and that card to every other card, so on
          if ((arr[i]%13 == arr[j]%13 && arr[j]%13 == arr[k]%13) && (i != j && j != k && i != k)){ // %13 to compare no matter what suit
            return true; // returns true if there is a three of a kind
          }
        }
      }
    } return false; // returns false if there is not
  }
  
  public static boolean DiamondFlush(int[] arr){ // method to check if a hand has all diamonds
    for (int i = 0; i < arr.length; i++){
      if (0 <= arr[i] && arr[i] <= 12) // makes sure each card is between 0 and 12 to make it a diamond
        return true; // returns true if each card is a diamond      
		} return false; // returns false if not
	}
	public static boolean ClubsFlush(int[] arr){ // method to check if a hand has all clubs
		for (int j = 0; j < arr.length; j++){
      if (13 <= arr[j] && arr[j] <= 25) // makes sure each card is between 13 and 25 to make it a club
        return true; // returns true if each card is a club
     } return false; // returns false if not
	}
	public static boolean HeartsFlush(int[] arr){ // method to check if a hand has all hearts
		for (int k = 0; k < arr.length; k++){
      if (26 <= arr[k] && arr[k] <= 38) // makes sure each card is between 26 and 38 to make it a heart
        return true; // returns true if each card is a heart
     } return false; // returns false if not
	}
	public static boolean SpadesFlush(int[] arr){ // method to check if a hand has all spades  
     for (int l = 0; l < arr.length; l++){
       if (39 <= arr[l] && arr[l] <= 51) // makes sure each card is between 39 and 51 to make it a spade
         return true; // returns true if each card is a spade     
      } return false; // returns false if not
    }
	
	public static boolean FullHouse(int[] arr){ // method to check for a full house
		if (ThreeOfAKind(arr) && Pair(arr)) // if there is both a three of a kind and a pair, aka a full house
			return true; // then it returns true
		return false; // if not, it returns false
	}  
	 
   public static int HighCard(int[] arr){ // this will be used as the default method if none of the other methods are present
      int highestValue = -1;
      for (int i = 0; i < arr.length; i++){
        if (highestValue < arr[i]){ // if the current highest value is less than the member of the array it's checking
          highestValue = arr[i]%13; // the new highest value is set to the value of that member of the array
        }
      
      }
     return highestValue; // returns the highest value in a hand
    } 
    
    public static void Win(int[] a, int[] b){ // method to decide the winner
      int a_result = 0;
      int b_result = 0;
     
      if (Pair(a)) // if there is a pair in player1's hand
        a_result = 1; // they get 1 point
      else if (Pair(b)) // if there is a pair in player2's hand
        b_result = 1; // they get 1 point
      else if (ThreeOfAKind(a)) // if there is a three of a kind in player1's hand
        a_result = 2; // they get 2 points
      else if (ThreeOfAKind(b)) // if there is a three of a kind in player2's hand
        b_result = 2; // they get 2 points
			else if (DiamondFlush(a)) // if all of the cards in player1's hand are diamonds
				a_result = 3; // they get 3 points
			else if (DiamondFlush(b)) // if all of the cards in player2's hand are diamonds
				b_result = 3; // they get 3 points
			else if (ClubsFlush(a)) // if all of the cards in player1's hand are clubs
				a_result = 3; // they get 3 points
			else if (ClubsFlush(b)) // if all of the cards in player2's hand are clubs
				b_result = 3; // they get 3 points
			else if (HeartsFlush(a)) // if all of the cards in player1's hand are hearts
				a_result = 3; // they get 3 points
			else if (HeartsFlush(b)) // if all of the cards in player2's hand are hearts
				b_result = 3; // they get 3 points
			else if (SpadesFlush(a)) // if all of the cards in player1's hand are spades
				a_result = 3; // they get 3 points
			else if (SpadesFlush(b)) // if all of the cards in player2's hand are spades
				b_result = 3; // they get 3 points
			else if (FullHouse(a)) // if there is a full house in player1's hand
				a_result = 4; // they get 4 points
			else if (FullHouse(b))
				b_result = 4; // they get 4 points 
      
			if (a_result > b_result)
        System.out.println("Player 1 wins!"); // player 1 wins if they have more points
      else if(b_result > a_result)
        System.out.println("Player 2 wins!"); // player 2 wins if they have more points
      else if (a_result == b_result){ // if they have the same amount of points, we check the highest card
        if (HighCard(a) > HighCard(b))
          System.out.println("Player 1 wins!"); // player1 wins if they have the highest card
        else if (HighCard(b) > HighCard(a))
          System.out.println("Player 2 wins!"); // player2 wins if they have the highest card
        else 
          System.out.println("Tie"); // if they have the same amount of points and same highest card, there's a tie
      }
   }
   
}