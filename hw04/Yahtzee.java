// Victoria Raso, 2/19/18, CSE2
// compute the scores and print out the following values
// upper section initial total, upper section total including bonus, lower section total, and grand total
import java.util.Scanner;

public class Yahtzee {
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); // declare an instance of the Scanner object and call the Scanner constructor
       
    // give the user the option to either roll randomly or to type in a 5 digit number representing the result of a specific roll
    
    System.out.println("Would you like to enter your own values for each dice or receive random values?");
    System.out.println("Enter 0 for your own values, and enter 1 for random");
    int preference = myScanner.nextInt();
    
    int die1 = 0;
    int die2 = 0;
    int die3 = 0;
    int die4 = 0;
    int die5 = 0;
    if (preference == 0){
      System.out.print("die 1: ");
      die1 = myScanner.nextInt();
      System.out.print("die 2: ");
      die2 = myScanner.nextInt();
      System.out.print("die 3: ");
      die3 = myScanner.nextInt();
      System.out.print("die 4: ");
      die4 = myScanner.nextInt();
      System.out.print("die 5: ");
      die5 = myScanner.nextInt();
    }
    else if (preference == 1){
      die1 = (int)((Math.random() * 6) + 1);
      System.out.println("die 1: " + die1); 
      die2 = (int)((Math.random() * 6) + 1);
      System.out.println("die 2: " + die2);
      die3 = (int)((Math.random() * 6) + 1);
      System.out.println("die 3: " + die3);
      die4 = (int)((Math.random() * 6) + 1);
      System.out.println("die 4: " + die4);
      die5 = (int)((Math.random() * 6) + 1);
      System.out.println("die 5: " + die5);  
    }
    else{
      System.out.println("Must enter 0 or 1");
    }   
       
    int upperTotal = 0;
    int upperBonusTotal = 0;
    int lowerTotal = 0;
    int grandTotal = 0;
    
    int ones = 0;
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0;
    
    // adds the value of die1 to a certain point category
    switch(die1){
      case 1:
        ones = ones + 1;
        break; 
      case 2:
        twos = twos + 2;
        break;
      case 3: 
        threes = threes + 3;
        break; 
      case 4:
        fours = fours + 4;
        break; 
      case 5:
        fives = fives + 5;
        break;
      case 6:
        sixes = sixes + 6;
        break;   
    }
    
    // adds the value of die2 to a certain point category
    switch(die2){
      case 1:
        ones = ones + 1;
        break;
      case 2:
        twos = twos + 2;
        break;
      case 3:
        threes = threes + 3;
        break;
      case 4:
        fours = fours + 4;
        break;
      case 5:
        fives = fives + 5;
        break;
      case 6:
        sixes = sixes + 6;
        break;
    }
    
    // adds the value of die3 to a certain point category
    switch (die3){
      case 1:
        ones = ones + 1;
        break;
      case 2:
        twos = twos + 2;
        break;
      case 3:
        threes = threes + 3;
        break;
      case 4:
        fours = fours + 4;
        break;
      case 5:
        fives = fives + 5;
        break;
      case 6:
        sixes = sixes + 6;
        break; 
    }
    
    // adds the value of die4 to a certain point category
    switch(die4){
      case 1:
        ones = ones + 1;
        break;
      case 2: 
        twos = twos + 2;
        break;
      case 3:
        threes = threes + 3;
        break;
      case 4:
        fours = fours + 4;
        break;
      case 5:
        fives = fives + 5;
        break;
      case 6:
        sixes = sixes + 6;
        break;        
    }
     
    // adds the value of die5 to a certain point category
    switch(die5){
      case 1:
        ones = ones + 1;
        break;
      case 2:
        twos = twos + 1;
        break;
      case 3:
        threes = threes + 3;
        break;
      case 4:
        fours = fours + 4;
        break;
      case 5:
        fives = fives + 5;
        break;
      case 6:
        sixes = sixes + 6;
        break;
    }
  
    // adds each point category to get upper total
    upperTotal = (ones + twos + threes + fours + fives + sixes);
    
    // adds bonus if earned
    if ( upperTotal >= 63) {
      upperBonusTotal = upperTotal + 35;
    }
    else {
      upperBonusTotal = upperTotal;
    }
    
    System.out.println("ones:" + ones + " twos:" + twos + " threes:" + threes + " fours:" + fours + " fives:" + fives + " sixes:" + sixes);
    System.out.println("Total for upper section: " + upperBonusTotal);
   
    // lower section totals!!
    
    // three of a kind possibilities 
    int threeOfAKind = 0;
    
    if (die1 == die2){
      if (die2 == die3 || die2 == die4 || die2 == die5){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die1 == die3){
      if (die3 == die2 || die3 == die4 || die3 == die5){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die1 == die4){
      if (die4 == die2 || die4 == die3 || die4 == die5){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die1 == die5){
      if (die5 == die2 || die5 == die3 || die5 == die4){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die2 == die3){
      if (die3 == die1 || die3 == die4 || die3 == die5){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die2 == die4){
      if (die4 == die1 || die4 == die3 || die4 == die5){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die2 == die5){
      if (die5 == die1 || die5 == die3 || die5 == die4){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die3 == die4){
      if (die4 == die1 || die4 == die2 || die4 == die5){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die3 == die5){
      if (die5 == die1 || die5 == die2 || die5 == die4){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    else if (die4 == die5){
      if (die5 == die1 || die5 == die2 || die5 == die3){
        threeOfAKind = die1 + die2 + die3 + die4 + die5;
      }
    }
    
    // four of a kind possibilities 
    int fourOfAKind = 0;
    if ( die1 == die2 && die2 == die3 && die3 == die4 ){
      fourOfAKind = die1 + die2 + die3 + die4 + die5;
    }
    else if ( die1 == die2 && die2 == die3 && die4 == die5 ){
      fourOfAKind = die1 + die2 + die3 + die4 + die5;
    }
    else if ( die1 == die2 && die3 == die4 && die4 == die5 ){
      fourOfAKind = die1 + die2 + die3 + die4 + die5;
    }
    else if (die2 == die3 && die3 == die4 && die4 == die5 ){
      fourOfAKind = die1 + die2 + die3 + die4 + die5;
    }
    
    // full house possibilities 
    int fullHouse = 0;
    
    if (die1 == die2 && die2 == die3){
      if (die4 == die5 && die4 != die1){
        fullHouse = 25;
      }     
    }
    else if(die1 == die2 && die2 == die4){
      if (die3 == die5 && die3 != die1){
        fullHouse = 25; 
      }      
    }
    else if( die1 == die2 && die2 == die5){
      if (die3 == die4 && die3 != die1){
        fullHouse = 25; 
      }      
    }
    else if(die1 == die3 && die3 == die4){
      if (die2 == die5 && die2 != die1){
        fullHouse = 25; 
      }      
    }
    else if(die1 == die3 && die3 == die5){
      if (die2 == die4 && die2 != die1){
        fullHouse = 25;
      }     
    }
    else if (die1 == die4 && die4 == die5){
      if (die2 == die3 && die2 != die1){
        fullHouse = 25;
      }       
    }
    else if (die2 == die3 && die3 == die4){
      if (die1 == die5 && die1 != die2){
        fullHouse = 25;
      }     
    }
    else if (die2 == die3 && die3 == die5){
      if (die1 == die4 && die1 != die2){
        fullHouse = 25;
      }     
    }
    else if (die2 == die4 && die4 == die5){
      if (die1 == die3 && die1 != die2){
        fullHouse = 25;
      }
    }
    else if (die3 == die4 && die4 == die5){
      if (die1 == die2 && die1 != die3){
        fullHouse = 25; 
      }
    }
    
    // small straight
    int smallStraight = 0;
    if (die1 != die2 && die1 != die3 && die1 != die4){
      if (die2 != die3 && die2 != die4){
        if (die3 != die4){
          if ((die1 + die2 + die3 + die4) == 10){
            smallStraight = 30;
          } // if 1,2,3,4 is rolled
          else if ((die1 + die2 + die3 + die4) == 14){
            smallStraight = 30;
          } // if 2,3,4,5 is rolled
          else if ((die1 + die2 + die3 + die4) == 18){
            smallStraight = 30;
          } // 3,4,5,6 is rolled
        }
      }
    } // die1, die2, die3, and die4 are the ones involved in the small straight
    if (die1 != die2 && die1 != die3 && die1 != die5){
      if (die2 != die3 && die2 != die5){
        if (die3 != die5){
          if ((die1 + die2 + die3 + die5) == 10){
            smallStraight = 30;
          } // if 1,2,3,4 is rolled
          else if ((die1 + die2 + die3 + die5) == 14){
            smallStraight = 30;
          } // if 2.3.4.5 is rolled
          else if ((die1 + die2 + die3 + die5) == 18){
            smallStraight =30;
          } // if 3.4.5.6 is rolled
        }
      }
    } // die1, die2, die3, and die5 are the ones invovled in the small straight
    if (die1 != die2 && die1 != die4 && die1 != die5){
      if (die2 != die4 && die2 != die5){
        if (die4 != die5){
          if ((die1 + die2 + die4 + die5) == 10){
            smallStraight = 30;
          }
          else if ((die1 + die2 + die4 + die5) == 14){
            smallStraight = 30;
          }
          else if ((die1 + die2 + die4 + die5) == 18){
            smallStraight = 30;
          }
        }
      }
    } // die1, die2, die4, and die5 are the ones invovled in the small straight
    if (die1 != die3 && die1 != die4 && die1 != die5){
      if (die3 != die4 && die3 != die5){
        if (die4 != die5){
          if ((die1 + die3 + die4 + die5) == 10){
            smallStraight = 30;
          }
          else if ((die1 + die3 + die4 + die5) == 14){
            smallStraight = 30;
          }
          else if ((die1 + die3 + die4 + die5) == 18){
            smallStraight = 30;
          }
        }
      }
    } // die1, die3, die4, and die5 are the ones invovled in the small straight
    if (die2 != die3 && die2 != die4 && die2 != die5){
      if (die3 != die4 && die3 != die5){
        if (die4 != die5){
          if ((die2 + die3 + die4 + die5) == 10){
            smallStraight = 30;
          }
          else if ((die2 + die3 + die4 + die5) == 14){
            smallStraight = 30;
          }
          else if ((die2 + die3 + die4 + die5) == 18){
            smallStraight = 30;
          }
        }
      }
    } // die2, die3, die4, and die5 are the ones involved in the small straight 
   
    // large straight possiblities 
    int largeStraight = 0;
    if (die1 != die2 && die1 != die3 && die1 != die4 && die1 != die5){
      if (die2 != die3 && die2 != die4 && die2 != die5){
        if (die3 != die4 && die3 != die5){
          if (die4 != die5){
            if ((die1 + die2 + die3 + die4 + die5) == 15){
              largeStraight = 40;
            } // if 1,2,3,4,5 is rolled
            else if ((die1 + die2 + die3 + die4 + die5) == 20){
              largeStraight = 40;
            } // if 2,3,4,5,6 is rolled
          }        
        }
      }
    } // makes sure no die is the same number and that it can equal the total of a straight of 1,2,3,4,5 or 2,3,4,5,6
       
    // yahtzee 
    int yahtzee = 0;
   
    if ( die1 == die2 && die2 == die3 && die3 == die4 && die4 == die5 ) {
      yahtzee = 50;
    }
    
    // chance 
    int chance = die1 + die2 + die3 + die4 + die5;
    
    lowerTotal = threeOfAKind + fourOfAKind + fullHouse + smallStraight + largeStraight + yahtzee + chance;
    
    System.out.print("3 of a kind:" + threeOfAKind);
    System.out.print(" 4 of a kind:" + fourOfAKind);
    System.out.print(" full house:" + fullHouse);
    System.out.print(" small straight:" + smallStraight);
    System.out.print(" large straight:" + largeStraight);
    System.out.print(" yahtzee:" + yahtzee);
    System.out.println(" chance:" + chance);
    System.out.println("Lower section total:" + lowerTotal);
    
    // calculate and print out the grand total
    grandTotal = upperBonusTotal + lowerTotal;
    
    if (grandTotal != 50){
      System.out.println("Grand total: " + grandTotal);
    } // the 75 is if the user enters an invalid preference digit aka not 0 or 1, so when the points are calculated as all 0's there is a "yahtzee" which is 50 points, but we don't want that to count for anything
    else{
      System.out.println("Try again");
    }
        
  }
}
