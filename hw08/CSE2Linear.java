// CSE2, 4/10/18
// sort grades in an array 
// I'm not sure how to get only one of the linear, binary, or scramble methods to run, so I was just commenting out the ones 
// I didn't want to test at that time

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {

	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		int grades[]=new int[15]; // create array
    System.out.println("Enter 15 ascending ints for final grades in CSE2"); // prompts the user to enter grades
		int x = -1; // to make sure the loop runs at least once 
		for (int i = 0; i < grades.length; i++)
		{
			while (x<0 || x>100)
			{
			    while (!scan.hasNextInt()){ // runs when the number entered isn't an integer
			      scan.next();
			      System.out.println("Please make sure it's an integer. ");
			    }
		    x = scan.nextInt(); // assigns value to x once we know it's an integer
		    if ((x<0 || x>100)){ // || (i < 0 && x < grades[i-1]) might help with making sure it's in ascending order
		    	System.out.println("Please make sure it's an integer from 0 - 100. ");
        }
        
        
			}
      /*if (i > 0 && grades[i-1] > grades[i]){
          System.out.println("Please make sure the grades are in ascending order."); 
          break; */
        } 
		    grades[i] = x; // assigns member in the array to the value given by the user if there are no errors
		    x = -1; // to make the loop run again
		  }
		//fix to check ascending
		System.out.println("Enter a grade to search for: "); // prompt the user
		int target = scan.nextInt(); // grade we want to search for 
		//binarySearch(grades, target); // prints answer using binary search
		scrambleArray(grades);
		//linearSearch(grades,target); // prints answer using linear search 
		
	}
	
	public static void binarySearch(int arr[], int key)
	    {
		int counter = 1;
	        int left = 0;
	        int right = arr.length - 1;
	        while (left <= right)
	        {
	            int middle = left + (right-left)/2;
	 
	            if (arr[middle] == key) // 
	            {
	                System.out.print(key + " was found with " + counter + " iterations.");
	                return;
	            }
	            if (arr[middle] < key) // ignores the left half 
	                left = middle + 1;
	            else // ignores the right half 
	                right = middle - 1;
	            counter++; // counts how many iterations it took 
	        }
	
	        System.out.println(key + " was not found."); // reach here if the number entered was not in the array
	    }
	
	
	public static void linearSearch(int arr[], int key)
	{
		int counter = 1;
		for (int i = 0; i < arr.length; i++)
		{
			if (key == arr[i]) 
			{
				System.out.print(key + " was found with " + counter + " iterations.");
				return; // stops the loop once the value was found in the array
			}
			counter++; // counts how many iterations it took 
		}
		System.out.println(key + " was not found."); // prints if the value in the array was not found	
	}

  /* public static void scrambleArray(int a[]) { // scrambled array 
        int n = a.length;
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(a, i, change);
        }
     for (int j = 0; j < n; j++){
       System.out.print(a[i] + " ");
     }
    
    }
   public static void swap(int a[], int w, int change) {
        int helper = a[w];
        a[w] = a[change];
        a[change] = helper;
    }
    */
	
}