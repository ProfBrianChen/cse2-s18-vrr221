public class lab09{
  public static void main(String[] args){
    int[] array0 = {5, 2, 7, 3, 8, 11, 1, 9};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    int[] array3;
    
    print(array0);
    inverter(array0);    
    print(array0);

    inverter2(array1);
    print(array1);

    array3 = inverter2(array2);
    print(array3);
  }
  
  public static int[] copy(int[] arr){
    int[] copy = new int[arr.length];
    for (int i = 0; i < arr.length; i++){
      copy[i] = arr[i];
    }
    return copy;
  }
  
  public static void inverter(int[] arr){
    int temp;
    for (int i = 0; i < arr.length/2; i++){
      temp = arr[arr.length - 1 -i];
      arr[arr.length - 1 - i] = arr[i];
      arr[i] = temp;
    } 
  }
  
  public static int[] inverter2(int[] arr){
    int[] newArray = copy(arr);
    for (int i = 0; i < arr.length; i++){
      inverter(arr);
      newArray = arr;
    }
    return newArray;
  } 
  
  public static void print(int[] arr){
    for (int i = 0; i < arr.length; i++){
      System.out.print(arr[i] + " ");
    }
    System.out.println();
  }
  
  
    
  
}