// Victoria Raso, 2/16/18, CSE2
// generate a random number, create two String variables: one for the name of the suit and one for the identity of the card
// Use if statements to assign the suit name
// Assign the card identity
// Print out the name of the randomly selected card

public class CardGenerator { 
  
  public static void main(String[] args) {
    
    double cardValue = (int)(Math.random() * 52);
    String suit = "";
    String identity = ""; 
    
    if ( 1 <= cardValue && cardValue <= 13 ) {
      suit = "Diamonds";
    }
    else if ( 14 <= cardValue && cardValue <= 26 ) {
      suit = "Clubs";
    }
    else if ( 27 <= cardValue && cardValue <= 39 ) {
      suit = "Hearts";
    }
    else if ( 40 <= cardValue && cardValue <= 52) {
      suit = "Spades";
    }
    
    int identityIndex = (int)cardValue%13;
    if ( identityIndex == 1 ) {
      identity = "Ace";
    }
    else if ( identityIndex == 11 ) {
      identity = "Jack";
    }
    else if ( identityIndex == 12 ) {
      identity = "Queen";
    }
    else if ( identityIndex == 13 ) {
      identity = "King";
    }
    else {
      identity = Integer.toString(identityIndex);
    } 
    System.out.println("You picked the " + identity + " of " + suit);
  }

}
