// Victoria Raso, 2/3/18, CSE2

public class WelcomeClass {
  public static void main(String[] args) {
   
    System.out.println("  -----------");
    // prints first line with correct spacing
    System.out.println("  | WELCOME |");
    // prints second line with correct spacing
    System.out.println("  -----------");
    // prints third line with correct spacing
    System.out.println("  ^  ^  ^  ^  ^  ^");
    // prints fourth line with correct spacing
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    // backslash needs 2 slashes to print out as 1; prints fifth line with correct spacing 
    System.out.println("<-V--R--R--2--2--1->");
    // prints sixth line with correct spacing
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); 
    // backslash needs 2 slahses to print out as 1; prints seventh line with correct spacing 
    System.out.println("  v  v  v  v  v  v");   
    // prints last line with correct spacing
  }
}